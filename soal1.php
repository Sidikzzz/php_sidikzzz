<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Soal1 | Sidik Permana</title>
	<style type="text/css">
		table {
            padding: 20px 10px;
            border: 1px solid;
        }
        input[type=text] {
            width: 80px;
            text-align: center;
        }
        .content {
        	height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .form{
        	width: 40%;
        }
	</style>
</head>
<body>
	<center>
		<div class="content">
			<form class="form" action="soal1.php" method="POST">
			<?php
				if(!isset($_POST['baris']) && !isset($_POST['Kolom']) && !isset($_POST["inputan"])){
			?>
			<table width="100%">
				<tr>
					<td width="40%">Inputkan Jumlah Baris</td>
					<td width="1%">:</td>
					<td><input type="text" name="baris" required> Contoh : 1</td>
				</tr>
				<tr>
					<td width="40%">Inputkan Jumlah Kolom</td>
					<td width="1%">:</td>
					<td><input type="text" name="kolom" required> Contoh : 3</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<input type="submit" value="SUBMIT">
					</td>
				</tr>
			</table>
			<?php
				}else{
					if(!isset($_POST["inputan"])){
			?>
					<table width="100%">
		                <?php foreach(range(1, $_POST['baris']) as $baris) { ?>
		                <tr>
		                    <?php foreach(range(1, $_POST['kolom']) as $kolom) { ?>
		                    <td>
		                        <label><?php echo $baris.".".$kolom ?></label>
		                        <input type="text" name="<?php echo "inputan[$baris][$kolom]" ?>">
		                    </td>
		                    <?php } ?>
		                </tr>
		                <?php } ?>
		                <tr>
		                    <td colspan=3 align="center">
		                        <input type="submit" value="SUBMIT">
		                    </td>
		                </tr>
		            </table>
            	<?php
					}else{
				?>
				<table>
		            <?php foreach($_POST["inputan"] as $row => $rowValue) { ?>
		                <tr>
		                	<td>
		                		<b>
				                    <?php foreach($rowValue as $column => $columnValue) { 
				                        echo $row.".".$column . ":" . $columnValue.'<br/>';
				                    } ?>
		                    	</b>
		                    </td>
		                </tr>
		            <?php } ?>
		        </table>
		        <a href="soal1.php">Kembali</a>
			<?php
					}
				}
			?>
			</form>
		</div>
	</center>
</body>
</html>