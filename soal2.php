<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Soal1 | Sidik Permana</title>
	<style type="text/css">
		table {
            padding: 20px 10px;
            border-collapse: collapse;
        }
        input[type=text] {
            width: 80%;
        }
        .content {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .form{
        	width: 40%;
        }
	</style>
</head>
<?php
        $conn = new mysqli("localhost","root","","testdb");

        $hobi = isset($_POST['hobi']) ? $_POST['hobi'] : '';

        $search = " WHERE hobi LIKE '%$hobi%'";

        $sql = "SELECT hobi.hobi
					 , COUNT(hobi.person_id) AS jumlah_person
				FROM hobi JOIN person ON (hobi.person_id = person.id)
				".$search."
				GROUP BY hobi.hobi HAVING COUNT(hobi.person_id) > 0 
				ORDER BY COUNT(hobi.person_id) DESC";
		$result = $conn->query($sql);
?>
<body>
	<center>
		<div class="content">
			<form class="form" action="soal2.php" method="POST">
				<h3>Search by hobi</h3>
				<table width="100%" border="1">
					<tr>
						<td>Hobi</td>
						<td><input type="text" name="hobi"> <input type="submit" value="SEARCH"></td>
					</tr>
				</table>
				<br/>
				<table width="100%" border="1">
	                <tr>
	                    <th width="10%">No</th>
	                    <th width="60%">Hobi</th>
	                    <th width="30%">Jumlah Person</th>
	                </tr>
	                <?php
	                	 
	                	if($result->num_rows > 0){
	                		$nomor = 1;
				            foreach($result as $row){
				                echo "
				                <tr>
				                    <td align=\"center\">".$nomor++."</td>
				                    <td>$row[hobi]</td>
				                    <td align=\"center\">$row[jumlah_person]</td>
				                </tr>";
				            }
				        }
				        else{
				            echo "<tr><td colspan='3'>Tidak ada data</td></tr>";
				        }
				    ?>
	            </table>
	            <br/>
	            <a href="testdb.sql" target="_blank">download struktur table dan data</a>
			</form>
		</div>
	</center>
</body>
</html>